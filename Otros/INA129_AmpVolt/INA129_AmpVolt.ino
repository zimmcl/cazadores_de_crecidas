#include <Wire.h>
#include <Adafruit_INA219.h> // You will need to download this library
#include <SPI.h>
#include <SD.h>

Adafruit_INA219 ina219; // Declare and instance of INA219

File Archivo;

float voltaje_shunt = 0;
float voltaje_bus = 0;
float corriente_mA = 0;
float voltaje_carga = 0;
float potencia = 0;
float energia = 0;
int medicion = 0;
String  parametros = "";


void setup(void)
{
  //Se esablece comunicación con el monitor serial para la comprobación de la
  //carga de datos y del sensado de corriente y tensión.
  Serial.begin(9600);

  //Se muestra por el monitor si la comunicación se ha establecido correctamente
  //o ha habido algún tipo de error.
  if (!SD.begin(4)) {
    Serial.println("Se ha producido un fallo al iniciar la comunicación");
    return;
  }
  Serial.println("Se ha iniciado la comunicación correctamente");


  //Se abre el documento sobre el que se va a leer y escribir.
  Archivo = SD.open("datos.csv", FILE_WRITE);

  parametros = "Medicion;Corriente[mA];Tension[V];Potencia[mW];Energia[Wh]";
  //Se comprueba que el archivo se ha abierto correctamente y se procede a
  //escribir en él.
  if (Archivo) {

    //Se escribe información en el documento de texto datos.txt
    Archivo.println(parametros);

    //Se cierra el archivo para almacenar los datos.
    Archivo.close();
  }

  //En caso de que haya habido problemas abriendo datos.txt, se muestra por pantalla.
  else {

    Serial.println("El archivo datos.txt no se abrió correctamente");
  }

  //Inicializamos el sensado de parametros
  ina219.begin();

}

void loop(void)
{
  parametros = "";
  medicion++;
  parametros += medicion;
  parametros += ";";
  voltaje_shunt = ina219.getShuntVoltage_mV();
  voltaje_shunt = abs(voltaje_shunt);
  voltaje_bus = ina219.getBusVoltage_V();
  voltaje_bus = abs(voltaje_bus);
  corriente_mA = ina219.getCurrent_mA();
  corriente_mA = abs(corriente_mA);

  parametros += corriente_mA;
  parametros += ";";

  voltaje_carga = voltaje_bus + (voltaje_shunt / 1000);

  parametros += voltaje_carga;
  parametros += ";";

  Serial.print("Voltage Shunt:  ");
  Serial.print(voltaje_shunt);
  Serial.println(" mV");

  Serial.print("Voltage bus:  ");
  Serial.print(voltaje_bus);
  Serial.println(" V");

  Serial.print("Corriente:  ");
  Serial.print(corriente_mA);
  Serial.println(" mA");

  Serial.print("Voltage carga:  ");
  Serial.print(voltaje_carga);
  Serial.println(" V");

  potencia = voltaje_carga * corriente_mA;

  parametros += potencia;
  parametros += ";";

  Serial.print("Potencia:  ");
  Serial.print(potencia);
  Serial.println(" mW");

  energia = energia + (potencia / 3600);

  parametros += energia;

  Serial.print("Energia:  ");
  Serial.print(energia);
  Serial.println(" mWh");

  Serial.println("");

  //****************************************ALMACENAMIENTO DE DATOS EN SD***********************************
  //Se abre el documento sobre el que se va a leer y escribir.
  Archivo = SD.open("datos.csv", FILE_WRITE);
  //Se comprueba que el archivo se ha abierto correctamente y se procede a
  //escribir en él.
  if (Archivo) {

    //Se escribe información en el documento de texto datos.txt
    Archivo.println(parametros);

    //Se cierra el archivo para almacenar los datos.
    Archivo.close();
  }

  //En caso de que haya habido problemas abriendo datos.txt, se muestra por pantalla.
  else {

    Serial.println("El archivo datos.txt no se abrió correctamente");
  }
  //*********************************************************************************************************

  delay(1000);

  if (medicion == 3600)
  {
    exit(0);
  }
}
