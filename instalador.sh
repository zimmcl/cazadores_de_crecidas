#! /bin/bash
acceso
tareas=9 
finalizadas=0
esperada=4.9.13-v7+
esperado=4.9.13-v7+
version=$(uname -r)

salida=$(lsmod | grep spi_)
var=$(cut -d'_' -f1 <<<$salida)
ssh=$(systemctl is-enabled ssh)
usuario=$(id -u -n)


version="${version%*-*}"  
version="${version//.}"   
esperado="${esperado%*-*}"
esperado="${esperado//.}" 

RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color


#Barra de progreso
prog() {
    local w=20 p=$1;  shift
    # create a string of spaces, then change them to dots
    printf -v dots "%*s" "$(( $p*$w/100 ))" ""; dots=${dots// /.};
    # print those dots on a fixed-width space plus the percentage etc. 
    printf "\r\e[K|%-*s| %3d%% %s" "$w" "$dots" "$p" "$*";
}

clear
echo ' = = ------------------------------------------------------- = = '
echo -e "              ${CYAN}INSTALADOR CAZADORES DE CRECIDAS v1.0${NC}"
echo ' = = ------------------------------------------------------- = = '

echo ''
if [ -e /etc/rpi-issue ]; then
	echo -e "${CYAN}# CARACTERISTICAS DEL SISTEMA${NC}"
	cat /etc/rpi-issue
fi
if [ -e /usr/bin/lsb_release ]; then
	echo -e "${CYAN}# SISTEMA OPERATIVO${NC}"
	lsb_release -irdc
fi
echo -e "${CYAN}# VERSION DEL KERNEL${NC}"
uname -r
echo -e "${CYAN}# MODELO HARDWARE${NC}"
cat /proc/device-tree/model && echo
echo -e "${CYAN}# NOMBRE DE USUARIO${NC}"
hostname
echo -e "${CYAN}# VERSION DEL FIRMWARE${NC}"
/opt/vc/bin/vcgencmd version
echo '-----------------------------------------------------------------'

echo 'La acción que emprenda el instalador dependerá de la opción seleccionada:'
echo ' * Para continuar con la instalación seleecion (S)'
echo ' * Para abortar la instalación seleccione (N)'
echo -n 'Desea continuar? (S/N) '
read acceso

if [[ $acceso = S ]]; then
	
	#echo '---------------Habilitando Perifércos---------------'
	prog "$finalizadas" Trabajando en [Habilitar Perifericos]
	finalizadas=$((finalizadas + 100/$tareas))
	sleep .1
	echo -e ''
	
	if [[ $var == 'spi' ]]; then
		echo -e " * [${GREEN}EXITO${NC}] Interfaz SPI habilitada." 
	else
		sleep 1
		echo "dtparam=spi=on" | sudo tee -a /boot/config.txt > /dev/null
		if [[ $var == 'spi' ]]; then
			echo -e " * [${GREEN}EXITO${NC}] Interfaz SPI habilitada."
		else
			echo -e " * [${RED}FALLO${NC}] Interfaz SPI en error."
		fi
	fi

	if [[ $ssh == 'enabled' ]]; then
		echo -e " * [${GREEN}EXITO${NC}] Interfaz SSH habilitada."
	else
		sleep 1
		sudo systemctl enable ssh &>/dev/null
		if [[ $ssh == 'enabled' ]]; then
			echo -e " * [${GREEN}EXITO${NC}] Interfaz SSH habilitada."
		else
			echo -e " * [${RED}FALLO${NC}] Interfaz SSH en error."
		fi
	fi
	
	prog "$finalizadas" Trabajando en [Crear Directorio]
	finalizadas=$((finalizadas + 100/$tareas))
	sleep .1
	echo -e ''
	if [ ! -d /home/$usuario/Desktop/CdC_Recursos ]; then
		mkdir -p /home/$usuario/Desktop/CdC_Recursos;
		if [ -d /home/$usuario/Desktop/CdC_Recursos ]; then
			echo -e " * [${GREEN}EXITO${NC}] Directorio creado."
		else
			echo -e " * [${RED}FALLO${NC}] No se pudo crear directorio."
		fi
	else
		echo -e " * [${GREEN}EXITO${NC}] Directorio existente."
	fi
	#echo -e ''
	
	prog "$finalizadas" Trabajando en [Instalar Python y Python3]
	finalizadas=$((finalizadas + 100/$tareas))
	sleep .1
	echo -e ''
	if command -v python &>/dev/null; then
		echo -e " * [${GREEN}EXITO${NC}] Python instalado."
	else
		echo -e " * [${RED}FALLO${NC}] Python no instalado. Desea instalarlo? (Si/No)"
	fi
	
	if command -v python3 &>/dev/null; then
		echo -e " * [${GREEN}EXITO${NC}] Python3 instalado."
	else
		echo -e " * [${RED}FALLO${NC}] Python3 no instalado. Desea instalarlo? (Si/No)"
	fi
	
	prog "$finalizadas" Trabajando en [Descargar Recursos de Repositorio]
	finalizadas=$((finalizadas + 100/$tareas))
	sleep .1
	wget -P /home/$usuario/Desktop/CdC_Recursos https://bitbucket.org/zimmcl/cazadores_de_crecidas/raw/ee6182f0374718162e87982a7ad4001377f79e41/ModReceptor/ReceiveArduino.py &>/dev/null
	wget -P /home/$usuario/Desktop/CdC_Recursos https://bitbucket.org/zimmcl/cazadores_de_crecidas/raw/ee6182f0374718162e87982a7ad4001377f79e41/ModReceptor/lib_nrf24.py &>/dev/null
	wget -P /home/$usuario/Desktop/CdC_Recursos https://bitbucket.org/zimmcl/cazadores_de_crecidas/raw/9fe8778f7051028bd80d51e0d9bb7ecbbf4e1742/ModReceptor/py-spidev.zip &>/dev/null
	unzip /home/$usuario/Desktop/CdC_Recursos/py-spidev.zip -d /home/$usuario/Desktop/CdC_Recursos &>/dev/null
	rm /home/$usuario/Desktop/CdC_Recursos/py-spidev.zip &>/dev/null
	echo -e ''
	echo -e " * [${GREEN}EXITO${NC}] Recursos descargados."
	
	
	prog "$finalizadas" Trabajando en [Instalar py-spidev]
	finalizadas=$((finalizadas + 100/$tareas))
	sleep .1
	sudo python /home/$usuario/Desktop/CdC_Recursos/py-spidev/setup.py install &>/dev/null 	# python2
	sudo python3 /home/$usuario/Desktop/CdC_Recursos/py-spidev/setup.py install &>/dev/null 	# python3
	echo -e ''
	echo -e " * [${GREEN}EXITO${NC}] Instaladas dependecias para spidev."
	
	prog "$finalizadas" Trabajando en [Analizar versión de Kernel]
	finalizadas=$((finalizadas + 100/$tareas))
	sleep .1
	echo -e ''
	
	if [ "$(($esperado))" = "$(($version))" ] || [ "$(($esperado))" -gt "$(($version))" ];then
		echo -e " * [${GREEN}EXITO${NC}] Versión de Kernel correcta."
		finalizadas=100
		prog "$finalizadas"; echo -e Trabajando en ["${GREEN}EXITO${NC}"]
	else
       echo -e " * [${RED}FALLO${NC}] Versión del Kernel instalada $(uname -r) es mayor a la requerida."
       echo "Se requiere establecer el Kernel a la version $esperada o inferior para el funcionamiento correcto de las librerias NRF24L01."
       echo -n "Desea continuar? (SI/No)"
       read acceso
       
       if [[ $acceso = SI ]]; then
		echo -e ''
		echo -e "${CYAN}INSTALANDO...${NC}"

		#Limpiar la Cache
		sudo apt-get clean
		prog "$finalizadas" Trabajando en [Limpiar Cache]
		finalizadas=$((finalizadas + 100/$tareas))
		sleep .1

		prog "$finalizadas" Trabajando en [Actualizar listado repositorio]
		finalizadas=$((finalizadas + 100/$tareas))
		sleep .1
		#Actualizar listado de repositorios y paquetes
		sudo apt-get update >/dev/null
		#Descarga e instala las últimas versiones de paquetes, dependencias y posiblemente el Kernel más reciente.
	
		prog "$finalizadas" Trabajando en [Configurando Kernel $esperado]
		finalizadas=$((finalizadas + 100/$tareas))
		sleep .1
		sudo rpi-update f4cb84915874f32d88e2bfe793620d7afe25c755 >/dev/null
	
		prog "$finalizadas" Trabajando en [Eliminando paquetes obsoletos]
		finalizadas=$((finalizadas + 100/$tareas))
		sleep .1
		#Elimina todos los paquetes obsoletos y que ya no se necesitan
		sudo apt-get autoremove -y >/dev/null
		prog "$finalizadas"; echo -e Trabajando en ["${GREEN}EXITO${NC}"]
		echo -e ''
		echo -e "Reinciando para finalizar instalación."
		sudo reboot
		else
			echo 'Abortando Instalación'
			exit
		fi
	fi
else
	echo 'Abortando Instalación'
	exit
fi

