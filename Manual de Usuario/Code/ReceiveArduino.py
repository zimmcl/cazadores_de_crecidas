import RPi.GPIO as GPIO
from lib_nrf24 import NRF24
import time
import spidev
import csv, operator
import datetime, time
import csv
import os.path

GPIO.setmode (GPIO.BCM)

pipes = [[0xE8, 0xE8, 0xF0, 0xF0, 0xE1], [0xF0, 0xF0, 0xF0, 0xF0, 0xE1]]

radio = NRF24(GPIO, spidev.SpiDev())
radio.begin(0,17)

radio.setPayloadSize(8)
radio.setChannel(0x76)
radio.setDataRate(NRF24.BR_1MBPS)
radio.setPALevel(NRF24.PA_MAX)

radio.setAutoAck(True)
#radio.enableDynamicPayloads()
radio.enableAckPayload()

radio.openReadingPipe(1, pipes[1])
radio.printDetails()
radio.startListening()

'''
Si el archivo no existe en el directorio, se crea y se
insertan las cabeceras. Si existe se abre con permiso de
escritura append.
'''
if os.path.exists("Mediciones.csv"):
	csv = open("Mediciones.csv", "ab")
else:
	csv = open("Mediciones.csv","wb")
	cabecera = "Distancia[CM];Fecha[A-M-D];hora[H:M:S]\n"
	csv.write(cabecera) 

while True:
    while not radio.available(0):
        time.sleep(1/100)

    receivedMessage = []
    
    radio.read(receivedMessage, radio.getDynamicPayloadSize())
    ts = time.time()
    tiempoFecha = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    tiempoHora = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    
    centimetros = 0
    centimetros = receivedMessage[0]
    if	receivedMessage[1] != 0:
    	centimetros = centimetros + receivedMessage[1]*255
    
    file = str(centimetros) + ";" + str(tiempoFecha) + ";" + str(tiempoHora) + "\n"
    csv.write(file)
    	
    print("Recibido: {} centrimetros - {} {} ".format(centimetros,tiempoFecha,tiempoHora))
    
