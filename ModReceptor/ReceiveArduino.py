#!usr/bin/python
import RPi.GPIO as GPIO
from lib_nrf24 import NRF24
import time
import spidev
import csv, operator
import datetime, time
import csv
import os.path

GPIO.setmode (GPIO.BCM)

pipes = [[0x0F, 0x0F, 0x43, 0x21, 0x02], [0xF0, 0xF0, 0xF0, 0xF0, 0xE1]]

radio = NRF24(GPIO, spidev.SpiDev())
radio.begin(0,17)

radio.setPayloadSize(8)
radio.setChannel(0x76)
radio.setDataRate(NRF24.BR_1MBPS)
radio.setPALevel(NRF24.PA_MAX)

radio.setAutoAck(True)
#radio.enableDynamicPayloads()
radio.setPayloadSize(5)
radio.enableAckPayload()

radio.openReadingPipe(1, pipes[0])
radio.printDetails()
radio.startListening()

'''
Si el archivo no existe en el directorio, se crea y se
insertan las cabeceras. Si existe se abre con permiso de
escritura append.
'''
if os.path.exists("Mediciones.csv"):
	csv = open("Mediciones.csv", "ab")
else:
	csv = open("Mediciones.csv","wb")
	cabecera = "Distancia[CM];Fecha[A-M-D];hora[H:M:S]\n"
	csv.write(cabecera) 

while True:
    while not radio.available(0):
        time.sleep(1/100)

    receivedMessage = []
    va = radio.read(receivedMessage, radio.getPayloadSize())
    #radio.read(receivedMessage, radio.getDynamicPayloadSize())
    if va != 0:
        ts = time.time()
        tiempoFecha = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
        tiempoHora = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
        
        mensaje = 0
        centimetros = 0
        voltaje_S = 0
        voltaje_B = 0
        mensaje = receivedMessage[0]
        
        if	receivedMessage[0] != 0:
            #centimetros = centimetros + (int(receivedMessage[1]) & 0b00000011)
            
            mensaje |= receivedMessage[1] << 8
            mensaje |= receivedMessage[2] << 16
            mensaje |= receivedMessage[3] << 24
            #centimetros |= receivedMessage[4] << 32
            
            centimetros = (mensaje & 0B1111111111)
            voltaje_S = float(((mensaje >> 10) & 0B1111111111)) / 100
            voltaje_B = float(((mensaje >> 20) & 0B1111111111)) / 100
           
            #centimetros = centimetros + int(receivedMessage[2])*1024
            #centimetros = centimetros + int(receivedMessage[3])*2048
            #centimetros = centimetros + int(receivedMessage[4])*4096
        
        file = str(centimetros) + ";" + str(tiempoFecha) + ";" + str(tiempoHora) + "\n"
        csv.write(file)
            
        print("Recibido: {} centrimetros - {} {} ".format(centimetros,tiempoFecha,tiempoHora))
        print("Voltaje_S: {}".format(voltaje_S))
        print("Voltaje_B: {}".format(voltaje_B))
        #print("{0:b}".format(centimetros))
    
