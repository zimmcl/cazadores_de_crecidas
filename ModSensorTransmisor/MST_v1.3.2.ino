
#include "LowPower.h"
#include <NewPing.h>
#include <Average.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

/*--------------------------------------------------------*/
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
/*--------------------------------------------------------*/

#define TRIG_PIN 2
#define ECHO_PIN 3
#define MAX_DISTANCE 400
#define PWM_PIN  6


// Lo primero es inicializar la libreria indicando los pins de la interfaz
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);
RF24 radio(9, 10);//CE,CS
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

/* 1-6 Pipes de 3-5 bytes
   Pipe 1 empleado para recepcion, 5bytes
   Pipe 2 empleado para transmicion, 5bytes
*/
const uint64_t pipe[2] =
{ 0x1234432101LL, 0x0F0F432102LL };
const uint8_t num_lecturas = 10; // Numero de muestras
//uint16_t static BUFFER;
uint32_t static BUFFER_bin;
//bool write_state;
double old_moda = 0;
double new_moda = 0;
double old_delta = 0;
double new_delta = 0;
double delta = 0;
double abs_delta = 0;
uint8_t estado = 0;
uint8_t control = 5;

float solar_volt = 0;   // Variable para el voltaje del Panel Solar
float bat_volt = 0;     // Variable para el voltaje de la Bateria
float muestra1 = 0;     // Lectura analogica pin A0 (Tension Panel)
float muestra2 = 0;     // Lectura analogica pin A1 (Tension Bateria)
//uint8_t pwm = 6;            // Salida al Mosfet conectado al pin 6 (PWM)
uint8_t porcentaje_carga = 0;

void
setup ()
{
  /* FRECUENCIA DE RELOJ 16Mhz
    Configuramos preescaler para frecuencia PWM (D5 y D6) de 61.04 Hz (PS: 1024)
    CAP. 12 User Manual -> Fast PWM: Frec_pwm = (Frec_clk/N*256)
                          N: valor de preescaler (PS: 64 default)
                          Frec_pwm default 976.56 Hz  (TCCR0B = TCCR0B & 0b11111000 | 0b00000011)
  */
  //TCCR0B = TCCR0B & 0b11111000 | 0b00000101;
  //TCCR0B |= 0b11111000 | 0x05;

  Serial.begin (9600);
  //SSD1306_EXTERNALVCC
  //SSD1306_SWITCHCAPVCC
  if (!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }

  oled.clearDisplay();
  delay(500);
  oled.setTextSize(1);             // Normal 1:1 pixel scale
  oled.setTextColor(WHITE);        // Draw white text
  oled.setCursor(0, 0);            // Start at top-left corner
  oled.println(F("CONFIGURACION INICIAL"));
  oled.display();

  delay (20);
  printf_begin ();                     //Debug
  radio.begin ();
  radio.setPALevel(RF24_PA_LOW);
  //radio.setDataRate(RF24_1MBPS);
  radio.setChannel (0x76);
  radio.enableAckPayload();
  radio.enableDynamicAck();
  radio.setAutoAck(1);
  radio.setRetries(2, 15);
  radio.openWritingPipe (pipe[1]);
  //radio.openReadingPipe (1,pipe[0]);
  //radio.enableDynamicPayloads ();
  radio.setPayloadSize(5);
  radio.powerUp ();
  radio.printDetails ();               //Debug
  delay (25);
  Sensado ();
  Sensado ();
  delay (25);

  pinMode (PWM_PIN, OUTPUT);
  digitalWrite (PWM_PIN, LOW);
  analogReference (DEFAULT);

}

/*---------------------------MAIN LOOP------------------------------*/
void
loop ()
{
  Funcionamiento_estandar ();
  Funcionamiento_continuo ();
  control = 5;
}

/*-----------------------------CHECK--------------------------------*/
//Nota: Probar el uso del resto para el chequeo de control
//Control = 1;
//if(delta > 5%control)
int
Check_delta ()
{
  delta += ((new_moda - old_moda) / new_moda) * 100;
  abs_delta = abs (delta);
  Serial.println ("Delta: " + String (abs_delta));
  delay (20);
  if (estado == 0 && abs_delta > 50)
  {
    estado = 1;
  }
  if (estado == 1 && control > 0)
  {
    delta = abs ((new_moda - old_moda) / new_moda) * 100;
    if (delta > 5)
    {
      control = 5;
    }
    else
    {
      control--;
    }
  }
  else
    estado = 0;
}

/*-----------------------------ESTANDAR--------------------------------/
  Funcionamiento normal. Rafaga de sensado y tranmision cada 1 minuto.*/
void
Funcionamiento_estandar ()
{
  while (!estado)
  {
    //i < 6
    delay (50);
    for (uint8_t i = 0; i < 1; i++)
    {
      LowPower.powerDown (SLEEP_4S, ADC_OFF, BOD_OFF);
    }
    delay (250);
    radio.powerUp();
    delay(250);
    Sensado ();
    Transmision ();
    Check_delta ();
    for (uint16_t i = 0; i < 10; i++)
    {
      Power_State ();
      delay(20);
    }
  }
}

/*-------------------------------CONTINUO-----------------------------------/
  Funcionamiento continuo. Rafaga de sensado y tranmision cada 2 segundos. */
void
Funcionamiento_continuo ()
{
  while (estado)
  {
    delay (250);
    LowPower.powerDown (SLEEP_1S, ADC_OFF, BOD_OFF);
    //LowPower.powerDown (SLEEP_4S, ADC_OFF, BOD_OFF);
    delay (250);
    radio.powerUp();
    delay (250);
    Sensado ();
    Transmision ();
    Check_delta ();
  }
}

/*-------------------------------SENSADO------------------------------------*/
void
Sensado ()
{
  Average<int> ave(num_lecturas);
  old_moda = new_moda;

  for (int i = 0; i < num_lecturas; i++)
  {
    int distancia = sonar.ping_cm ();
    ave.push (distancia);
    delay (20);
  }
  new_moda = ave.mode ();
}

/*-------------------------------TRANSMISION------------------------------------*/
void
Transmision ()
{
  BUFFER_bin = 0;

  oled.clearDisplay();
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  oled.setCursor(0, 0);

  oled.println(F("DISTANCIA:    "));
  oled.setCursor(85, 0);
  oled.println((int)new_moda);
  oled.setCursor(120, 0);

  BUFFER_bin = (uint16_t) new_moda;

  oled.setCursor(0, 10);
  oled.println(F("VOLT PANEL:   "));
  oled.setCursor(85, 10);
  oled.println(solar_volt);
  oled.setCursor(120, 10);

  delay(50);
  BUFFER_bin |= ((uint32_t)(solar_volt * 100) << 10);
  delay(50);

  oled.setCursor(0, 20);
  oled.println(F("VOLT BATERIA: "));
  oled.setCursor(85, 20);
  oled.println(bat_volt);
  oled.setCursor(120, 20);

  delay(50);
  BUFFER_bin |= ((uint32_t)(bat_volt * 100) << 20);
  delay(50);

  Serial.println(BUFFER_bin, BIN);
  Serial.println(BUFFER_bin, DEC);
  Serial.print("Distancia: ");
  Serial.println((uint16_t)new_moda);
  Serial.print("Solar_volt:");
  Serial.println((uint16_t)(solar_volt * 100));
  Serial.print("Bat_volt:");
  Serial.println((uint16_t)(bat_volt * 100));

  delay(50);
  radio.write (&BUFFER_bin, sizeof(BUFFER_bin), 0);
  //radio.startListening();
  bool write_state = radio.writeFast (&BUFFER_bin, sizeof(BUFFER_bin));
  delay(500);
  //Serial.print("ESCRITURA: ");
  //Serial.println(write_state);

  /*
      if (write_state) {
        oled.setCursor(0, 40);
        oled.println(F("ENVIADO CON EXITO"));
      }
      else {
        oled.setCursor(0, 40);
        oled.println(F("FALLO AL ENVIAR"));
      }
  */

  /*
    while (!radio.available())
    {
    oled.setCursor(0, 50);
    oled.println(F("Esperando ACK"));
    //Serial.println("Esperando ACK");
    oled.display();
    delay(100);
    }

    radio.stopListening();
    oled.setCursor(0, 50);
    oled.println(F("ACK Ready"));
    Serial.println("ACK Ready");
  */
  oled.display();
  delay(1000);
}

/*-------------------------------PANEL - BATERIA----------------------------------*/
void
Power_State ()
{
  /*---------------------------- SENSADOR DE VOLTAJE -----------------------------*/
  /*Se obtiene 150 muestras de cada variable analogica y luego se calcula su media*/
  for (uint8_t i = 0; i < 150; i++)
  {
    muestra1 += analogRead (A0); // Leer el voltaje de entrada del Panel Solar
    muestra2 += analogRead (A1); // Leer el voltaje de entrada de la Bateria
    delay (2);
  }
  muestra1 = muestra1 / 150;
  muestra2 = muestra2 / 150;

  //1.95V ~= 400ADC -> 1ADC ~=4.87mV
  //5V/1.95 ~= 2.564

  double dif = ((2.564 - 2.132) + 0.30);

  //1.58V ~= 331ADC -> 1ADC ~=4.77mV
  //5V/1.58 ~= 3.1645

  //1.98V ~= 411ADC -> 1ADC ~=4.817mV
  //5V/1.58 ~= 3.101
  bat_volt = (muestra2 * 4.87 * (2.564 + dif)) / 1000;

  //1.58V ~= 329ADC -> 1ADC ~=4.795mV
  //5V/1.58 ~= 3.1645
  solar_volt = (muestra1 * 4.795 * (2.564 + dif)) / 1000;

  /*---------------------------- PWM BASED CHARGING -----------------------------*/
  /*A medida que la bateria se carga gradualmente, la tasa de carga (pwm duty) disminuye.
    7.1[V] = Completamente cargada(100%).
    5[V] = Completamente descargada(70%). Limite de descarga profunda del (30%)
  */
  //uint8_t duty = 0;
  if (solar_volt > 6 && (solar_volt > bat_volt) && (bat_volt <= 6.9)
      && bat_volt > 5)
  {
    analogWrite (PWM_PIN, 229); // @ 90% CdT // Carga de refuerzo // La mayor parte de la carga se realiza aqui
    //duty = 90;
  }
  else if (solar_volt > 6 && (solar_volt > bat_volt) && (bat_volt > 6.9)
           && (bat_volt <= 7.1))
  {
    analogWrite (PWM_PIN, 25.5); // 10% CdT // Carga flotante
    //duty = 10;
  }
  // Se deshabilita la carga cuando la bateria esta completamente cargada o cuando la luz del sol es insuficiente.
  else if ((bat_volt > 7.1) or (solar_volt < bat_volt) or solar_volt <= 6)
  {
    analogWrite (PWM_PIN, 0);
    //duty = 0;
  }
  //Serial.println ("");

  /*oled.setCursor(0, 40);
    oled.println(F("PWM DUTY:    "));
    oled.setCursor(85, 40);
    oled.println(duty);
    oled.display();*/

  /*---------------------------- INDICADOR DE ESTADO DE BATERIA -----------------------------*/
  /* La funcion map() usa matematica de numeros enteros, asi que se multiplica el voltaje de la bateria
    por 10 para convertirlo en un valor de numero entero.
    Cuando la tension de la baterria es de 5.5 voltios, esta totalmente descargada ( 5.5*10 = 55). Profundidad de descarga para maximizar vida util de la bateria
    Cuando el voltaje de la bateria es de 7.1 voltios, esta completamente cargada (7.1*10 = 71).
  */
  porcentaje_carga = bat_volt * 10;
  porcentaje_carga = map (bat_volt * 10, 55, 71, 0, 100);
  delay(10);
}

//Debug
int
serial_putc (char c, FILE *)
{
  Serial.write (c);
  return c;
}

//Debug
void
printf_begin (void)
{
  fdevopen (&serial_putc, 0);
}
